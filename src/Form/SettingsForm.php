<?php

namespace Drupal\simplifying\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simplifying\Services\Toolbar;
use Drupal\simplifying\Services\EntityFields;
use Drupal\simplifying\Services\SettingsActions;

/**
 * {@inheritdoc}
 */
class SettingsForm extends FormBase {

  /**
   * Add services toolbar.
   *
   * @var \Drupal\simplifying\Services\Toolbar
   */
  protected $toolbar;

  /**
   * Add services entity fields.
   *
   * @var \Drupal\simplifying\Services\EntityFields
   */
  protected $entityfields;

  /**
   * Add services settings actions.
   *
   * @var \Drupal\simplifying\Services\SettingsActions
   */
  protected $settingsactions;

  /**
   * Add __construct.
   *
   * @param \Drupal\simplifying\Services\Toolbar $toolbar
   *
   *   Add Toolbar.
   * @param \Drupal\simplifying\Services\EntityFields $entityfields
   *
   *   Add EntityFields.
   * @param \Drupal\simplifying\Services\SettingsActions $settingsactions
   *
   *   Add SettingsActions.
   */
  public function __construct(Toolbar $toolbar, EntityFields $entityfields, SettingsActions $settingsactions) {
    $this->toolbar = $toolbar;
    $this->entityfields = $entityfields;
    $this->settingsactions = $settingsactions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('simplifying.toolbar'),
      $container->get('simplifying.entity'),
      $container->get('simplifying.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simplifying_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_id = 'simplifying_settings_form_wrapper';
    $form['#prefix'] = '<div id="' . $form_id . '">';
    $form['#suffix'] = '</div>';

    $form['tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    $this->toolbar->formFields($form, $form_state);
    $this->entityfields->formFields($form, $form_state);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $menu_links = $form_state->getValue('menu_links');
    $menu_links = array_filter($menu_links);
    $this->settingsactions->setSettings('menu_links', array_keys($menu_links));

    $entity_fields = $form_state->getValue('entity_fields');
    $entity_fields = array_map('array_filter', $entity_fields);
    $this->settingsactions->setSettings('entity_fields', $entity_fields);

    $toolbar_tabs = $form_state->getValue('toolbar_tabs');
    $this->settingsactions->setSettings('toolbar_tabs', $toolbar_tabs);

    $design = $form_state->getValue('design');
    $this->settingsactions->setSettings('design', $design);

    $this->toolbar->createCss($design);

    $this->messenger()->addStatus($this->t('Settings saved successfully'));
  }

}
