<?php

namespace Drupal\simplifying\Services;

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Class Toolbar.
 *
 * @package Drupal\simplifying
 */
class Toolbar {

  use StringTranslationTrait;

  /**
   * Add services settings actions.
   *
   * @var \Drupal\simplifying\Services\SettingsActions
   */
  protected $settingsactions;

  /**
   * Add module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $modulehandler;

  /**
   * Add menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menutree;

  /**
   * Add current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentroutematch;

  /**
   * Add __construct.
   *
   * @param \Drupal\simplifying\Services\SettingsActions $settingsactions
   *
   *   Add SettingsActions.
   * @param \Drupal\Core\Extension\ModuleHandler $modulehandler
   *
   *   Add ModuleHandler.
   * @param \Drupal\Core\Menu\MenuLinkTree $menutree
   *
   *   Add MenuLinkTree.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentroutematch
   *
   *   Add CurrentRouteMatch.
   */
  public function __construct(SettingsActions $settingsactions, ModuleHandler $modulehandler, MenuLinkTree $menutree, CurrentRouteMatch $currentroutematch) {
    $this->settingsactions = $settingsactions;
    $this->modulehandler = $modulehandler;
    $this->menutree = $menutree;
    $this->currentroutematch = $currentroutematch;
  }

  /**
   * Get settings toolbar form fields.
   */
  public function formFields(&$form, $form_state) {
    $form['toolbar_design_wrapper'] = [
      '#type'  => 'details',
      '#title' => $this->t('Toolbar design'),
      '#group' => 'tabs',
    ];
    $defs = $this->settingsactions->getSettings('design');

    $form['toolbar_design_wrapper']['small_button'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Small button'),
      '#parents'       => ['design', 'small_button'],
      '#default_value' => !empty($defs['small_button']) ? 1 : 0,
    ];
    $form['toolbar_design_wrapper']['top_background'] = [
      '#type'          => 'color',
      '#title'         => $this->t('Top menu background'),
      '#parents'       => ['design', 'top_background'],
      '#default_value' => !empty($defs['top_background']) ? $defs['top_background'] : '#3A9F00',
    ];
    $form['toolbar_design_wrapper']['top_color'] = [
      '#type'          => 'color',
      '#title'         => $this->t('Top menu color'),
      '#parents'       => ['design', 'top_color'],
      '#default_value' => !empty($defs['top_color']) ? $defs['top_color'] : '#000000',
    ];
    $form['toolbar_design_wrapper']['submenu_background'] = [
      '#type'          => 'color',
      '#title'         => $this->t('Submenu background'),
      '#parents'       => ['design', 'submenu_background'],
      '#default_value' => !empty($defs['submenu_background']) ? $defs['submenu_background'] : '#555555',
    ];

    $form['toolbar_tabs_wrapper'] = [
      '#type'  => 'details',
      '#title' => $this->t('Toolbar tabs'),
      '#group' => 'tabs',
    ];
    $form['toolbar_tabs_wrapper']['toolbar_tabs'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Toolbar tabs'),
      '#title_display' => 'invisible',
      '#options'       => $this->getDefaultToolbarTabs(),
      '#default_value' => $this->getToolbarTabs(),
    ];

    $menu_tree_parameters = new MenuTreeParameters();
    $tree = $this->menutree->load('admin', $menu_tree_parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menutree->transform($tree, $manipulators);
    $options = [];
    $tree_rout = '';
    $this->treeOptions($tree, $options, $tree_rout);
    $defs = $this->settingsactions->getSettings('menu_links');
    
    $form['menu_links'] = [
      '#type'  => 'details',
      '#title' => $this->t('Menu links'),
      '#group' => 'tabs',
    ];
    $checkboxses = [];
    foreach ($options as $key => $val) {
      $disabled = in_array($val['path'], $checkboxses) ? TRUE : FALSE;
      $checkboxses[] = $val['path'];
      $form['menu_links'][$key] = [
        '#prefix'   => Markup::create('<div style="padding-left: ' . (($val['depth'] - 1) * 20) . 'px;">'),
        '#suffix'   => '</div>',
        '#type'     => 'checkbox',
        '#parents'  => ['menu_links', $val['path']],
        '#title'    => $val['title'],
        '#disabled' => $disabled,
      ];
      if (in_array($val['path'], $defs)) {
        $form['menu_links'][$key]['#default_value'] = 1;
      }
    }
  }

  /**
   * Get menu tree options.
   */
  public function treeOptions($tree, &$options, $tree_rout) {
    if (isset($tree['system.admin'])) {
      $tree = ['system.admin' => $tree['system.admin']];
    }
    foreach ($tree as $rout => $val) {
      $new_tree_rout = $tree_rout.' | '.$rout;
      if (!empty($val->link)) {
        $link = $val->link;
        if($link->getRouteName()){
          $path = Url::fromRoute($link->getRouteName(), $link->getRouteParameters(), $link->getOptions())->getInternalPath();
          if (!empty($path)) {
            $title = $link->getTitle();
            $options[$new_tree_rout] = [
              'title' => $title,
              'depth' => $val->depth,
              'path'  => $path,
              'rout'  => $rout,
            ];
          }
        }
      }
      if (!empty($val->subtree)) {
        $this->treeOptions($val->subtree, $options, $new_tree_rout);
      }
    }
  }

  /**
   * Add changes for toolbar.
   */
  public function toolbarTabs(&$items) {
    $design = $this->settingsactions->getSettings('design');
    $route_name = $this->currentroutematch->getRouteName();
    
    if($this->modulehandler->moduleExists('webform') || $this->modulehandler->moduleExists('comment')){
      $class = ['toolbar-icon', 'toolbar-icon-simplifying_unread'];
      $tab_name = $this->t('Form applications');
      $items['simplifying_unread'] = [
        '#type'               => 'toolbar_item',
        '#cache'              => [ 'max-age' => 0 ],
        '#wrapper_attributes' => [
          'class' => ['toolbar_simplifying_unread'],
        ],
        'tray'                => [
          '#heading'         => $tab_name,
          'unread_menu' => [
            '#theme'      => 'item_list',
            '#items'      => [],
            '#attributes' => [ 'class' => ['toolbar-menu', 'simplifying-unread-menu'] ],
          ],
        ],
        '#weight'             => 1000,
        'tab'                 => [
          '#type'       => 'html_tag',
          '#tag'        => 'button',
          '#value'      => $tab_name,
          '#attributes' => [ 'class'   => $class ],
        ],
      ];
      $unread_count = 0;
  
      if($this->modulehandler->moduleExists('webform')){
        $webforms = \Drupal::entityTypeManager()->getStorage('webform')->loadMultiple();
        foreach($webforms as $webform){
          $query = \Drupal::database()->select('simplifying_entity_unread', 'u');
          $query->condition('u.entity_type', 'webform_submission');
          $query->condition('u.bundle', $webform->id());
          $count = $query->countQuery()->execute()->fetchField();
          $unread_count += $count;

          $class = [];
          if ($route_name == 'entity.webform.results_submissions') {
            $route_webform = $this->currentroutematch->getParameter('webform');
            if(isset($route_webform) && $route_webform->id() == $webform->id()){
              $class[] = 'is-active';
            }
          }
          $items['simplifying_unread']['tray']['unread_menu']['#items'][ $webform->id() ] = [
            '#type'       => 'link',
            '#title'      => $webform->get('title').(!empty($count) ? ' ('.$count.')' : ''),
            '#url'        => Url::fromRoute('entity.webform.results_submissions', ['webform' => $webform->id()]),
            '#attributes' => [ 'class' => $class ],
            '#wrapper_attributes' => [
              'class' => ['menu-item', 'menu-item--'.$webform->id()],
            ],
          ];
        }
        ksort($items['simplifying_unread']['tray']['unread_menu']['#items']);
      }
      
      if($this->modulehandler->moduleExists('comment')){
        $query = \Drupal::database()->select('simplifying_entity_unread', 'u');
        $query->condition('u.entity_type', 'comment');
        $count = $query->countQuery()->execute()->fetchField();
        $unread_count += $count;

        $class = [];
        if ($route_name == 'comment.admin') {
          $class[] = 'is-active';
        }
        $items['simplifying_unread']['tray']['unread_menu']['#items']['comment'] = [
          '#type'       => 'link',
          '#title'      => $this->t('Comments').(!empty($count) ? ' ('.$count.')' : ''),
          '#url'        => Url::fromRoute('comment.admin'),
          '#attributes' => [ 'class' => $class ],
          '#wrapper_attributes' => [
            'class' => ['menu-item', 'menu-item--comment'],
          ],
        ];
      }
      if(!empty($unread_count)){
        $items['simplifying_unread']['tab']['#value'] .= ' ('.$unread_count.')';
      }
    }

    if (!file_exists('public://simplifying-ui.css')) {
      $this->createCss($design);
    }
    
    $class = ['toolbar-icon', 'toolbar-icon-simplifying_switch'];
    $tab_name = $this->t('Glyanec');
    $items['simplifying'] = [
      '#type'               => 'toolbar_item',
      '#cache'              => [ 'max-age' => 0 ],
      '#wrapper_attributes' => [
        'class' => ['toolbar_simplifying', 'toolbar_simplifying_switch'],
      ],
      'tray'                => [
        '#heading'         => $tab_name,
        'simplifying_menu' => [
          '#theme'      => 'item_list',
          '#items'      => [],
          '#attributes' => [ 'class' => ['toolbar-menu', 'simplifying-menu'] ],
        ],
      ],
      '#attached'           => [
        'library' => [
          'simplifying/simplifying_ui',
          'simplifying/simplifying'
        ],
      ],
      '#weight'             => 1001,
      'tab'                 => [
        '#type'       => 'html_tag',
        '#tag'        => 'button',
        '#value'      => $tab_name,
        '#attributes' => [ 'class'   => $class ],
      ],
    ];

    if (!empty($design['small_button'])) {
      $items['simplifying']['tab']['#attributes']['class'][] = 'small_button';
      $items['simplifying']['tab']['#attributes']['title'] = $tab_name;
    }
    $items['simplifying']['#attached']['drupalSettings']['simplifying_menu_links'] = $this->settingsactions->getSettings('menu_links');

    $toolbar_tabs = $this->getToolbarTabs();

    // Alter to hide the toolbar tabs.
    $this->modulehandler->alter('simplifying_hide_toolbar_tabs', $toolbar_tabs);
    $items['simplifying']['#attached']['drupalSettings']['simplifying_menu_tabs'] = $toolbar_tabs;

    $class = ['toolbar-icon', 'toolbar-icon-simplifying_minify'];
    $text = $this->t('Full administration');
    if (!empty($_COOKIE['simplifying'])) {
      $class[] = 'is-active';
      // $text = $this->t('Small administration');
      $text .= ' ('.$this->t('on').')';
    }else{
      $text .= ' ('.$this->t('off').')';
    }
    $items['simplifying']['tray']['simplifying_menu']['#items']['minify'] = [
      '#type'       => 'link',
      '#title'      => $text,
      '#url'        => Url::fromUserInput('#'),
      '#attributes' => [
        'class'   => $class,
        'onclick' => 'SimplifyingSwitch(event, this)',
      ],
      '#wrapper_attributes' => [
        'class' => ['menu-item', 'menu-item--minify'],
      ],
    ];

    $class = ['toolbar-icon', 'toolbar-icon-simplifying_services'];
    if ($route_name == 'simplifying.services') {
      $class[] = 'is-active';
    }
    $items['simplifying']['tray']['simplifying_menu']['#items']['services'] = [
      '#type'       => 'link',
      '#title'      => $this->t('Order additional services'),
      '#url'        => Url::fromRoute('simplifying.services'),
      '#attributes' => ['class' => $class],
      '#wrapper_attributes' => [
        'class' => ['menu-item', 'menu-item--services'],
      ],
    ];

    $class = ['toolbar-icon', 'toolbar-icon-simplifying_training'];
    if ($route_name == 'simplifying.training') {
      $class[] = 'is-active';
    }
    $items['simplifying']['tray']['simplifying_menu']['#items']['training'] = [
      '#type'       => 'link',
      '#title'      => $this->t('Training'),
      '#url'        => Url::fromRoute('simplifying.training'),
      '#attributes' => ['class' => $class],
      '#wrapper_attributes' => [
        'class' => ['menu-item', 'menu-item--training'],
      ],
    ];
  }

  /**
   * Get settings toolbar tabs.
   */
  public function getToolbarTabs() {
    $toolbar_tabs = $this->settingsactions->getSettings('toolbar_tabs');
    if (empty($toolbar_tabs)) {
      $toolbar_tabs = ['devel', 'user', 'administration_search'];
    }
    return array_filter($toolbar_tabs);
  }

  /**
   * Get default toolbart tabs.
   */
  public function getDefaultToolbarTabs() {
    return [
      'home'                  => $this->t('Back to site'),
      'administration'        => $this->t('Manage'),
      'shortcuts'             => $this->t('Shortcuts'),
      'user'                  => $this->t('User'),
      'devel'                 => $this->t('Devel'),
      'contextual'            => $this->t('Edit'),
      'administration-search' => $this->t('Administration search'),
    ];
  }

  /**
   * Entity insert actions.
   */
  public function entityInsert($entity) {
    $entity_type_id = $entity->getEntityTypeId();
    if($entity_type_id == 'webform_submission' || $entity_type_id == 'comment'){
      $entity_bundle = $entity->bundle();
      $query = \Drupal::database()->insert('simplifying_entity_unread');
      $query->fields([
        'entity_type' => $entity_type_id,
        'bundle'      => $entity_bundle,
        'entity_id'   => $entity->id(),
      ]);
      $query->execute();
    }
  }

  /**
   * Entity delete actions.
   */
  public function entityDelete($entity_type, $bundle, $entity_id) {
    $query = \Drupal::database()->delete('simplifying_entity_unread');
    $query->condition('entity_type', $entity_type);
    $query->condition('bundle', $bundle);
    $query->condition('entity_id', $entity_id);
    $query->execute();
  }

  /**
   * Entity Reading actions.
   */
  public function entityReadingRoute() {
    switch($this->currentroutematch->getRouteName()){
      case 'entity.webform.results_submissions':
        $webform = $this->currentroutematch->getParameter('webform');
        if(!empty($webform)){
          $query = \Drupal::database()->delete('simplifying_entity_unread');
          $query->condition('entity_type', 'webform_submission');
          $query->condition('bundle', $webform->id());
          $query->execute();
        }
      break;
      case 'comment.admin':
      case 'comment.admin_approval':
        $query = \Drupal::database()->delete('simplifying_entity_unread');
        $query->condition('entity_type', 'comment');
        $query->execute();
      break;
    }
  }
  /**
   * Create css.
   */
  public function createCss($design = []) {
    $css  = ':root {'.PHP_EOL;
    $css .= '  --topBackground: '.($design['top_background'] ?? '#3A9F00').';'.PHP_EOL;
    $css .= '  --topColor: '.($design['top_color'] ?? '#000000').';'.PHP_EOL;
    $css .= '  --submenuBackground: '.($design['submenu_background'] ?? '#555555').';'.PHP_EOL;
    $css .= '}'.PHP_EOL;
    file_put_contents('public://simplifying-ui.css', $css);
  }
}
