<?php

namespace Drupal\simplifying\Services;

use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Extension\ModuleHandler;

/**
 * Class EntityFields.
 *
 * @package Drupal\simplifying
 */
class EntityFields {

  use StringTranslationTrait;

  /**
   * Add services settings actions.
   *
   * @var \Drupal\simplifying\Services\SettingsActions
   */
  protected $settingsactions;

  /**
   * Add module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $modulehandler;

  /**
   * Add __construct.
   *
   * @param \Drupal\simplifying\Services\SettingsActions $settingsactions
   *
   *   Add SettingsActions.
   * @param \Drupal\Core\Extension\ModuleHandler $modulehandler
   *
   *   Add ModuleHandler.
   */
  public function __construct(SettingsActions $settingsactions, ModuleHandler $modulehandler) {
    $this->settingsactions = $settingsactions;
    $this->modulehandler = $modulehandler;
  }

  /**
   * Get settings entity form fields.
   */
  public function formFields(&$form, $form_state) {
    $types = [
      'nodes'    => $this->t('Node fields'),
      'users'    => $this->t('User fields'),
      'comments' => $this->t('Comment fields'),
      'taxonomy' => $this->t('Taxonomy fields'),
      'blocks'   => $this->t('Block fields'),
    ];
    foreach ($types as $type => $label) {
      $form[$type . '_wrapper'] = [
        '#type'  => 'details',
        '#title' => $label,
        '#group' => 'tabs',
      ];
      $form[$type . '_wrapper'][$type . '_fields'] = [
        '#type'          => 'checkboxes',
        '#title'         => $label,
        '#title_display' => 'invisible',
        '#parents'       => ['entity_fields', $type],
        '#options'       => $this->getDefaultFields($type),
        '#default_value' => $this->getFields($type),
      ];
    }
  }

  /**
   * Hide entity form fields.
   */
  public function hideFields(&$form, $type) {
    if (!empty($_COOKIE['simplifying'])) {
      return;
    }
    $fields = $this->getFields($type);
    foreach ($fields as $field) {
      $this->hideField($form, $field);
      if ($field == 'field_comments') {
        $this->hideField($form, 'comment');
      }
    }
  }

  /**
   * Hide entity form field.
   */
  public function hideField(&$form, $field) {
    // Alter to change the list of content fields to be hidden.
    $this->modulehandler->alter('simplifying_hide_field', $form, $field);

    if (!isset($form[$field]) && $field != 'format') {
      return;
    }
    if ($field == 'format') {
      $this->hideTextFormatElements($form);
    }
    elseif ($field == 'path' || $field == 'comment') {
      $form[$field]['widget'][0]['#prefix'] = '<div class="hidden">';
      $form[$field]['widget'][0]['#suffix'] = '</div>';
    }
    else {
      $form[$field]['#prefix'] = '<div class="hidden">';
      $form[$field]['#suffix'] = '</div>';
    }
  }

  /**
   * Hide entity form text format elements.
   */
  public function hideTextFormatElements(&$form) {
    foreach (Element::children($form) as $key) {
      if (!isset($form[$key]['#type']) || $form[$key]['#type'] == 'container') {
        $this->hideTextFormatElements($form[$key]);
      }
      elseif ($form[$key]['#type'] == 'text_format') {
        $form[$key]['#after_build'][] = __CLASS__ . '::hideTextFormatElement';
      }
    }
  }

  /**
   * Hide entity form text format element.
   */
  public static function hideTextFormatElement($element) {
    if (!empty($element['format'])) {
      $element['format']['#prefix'] = '<div class="hidden">';
      $element['format']['#suffix'] = '</div>';
    }
    return $element;
  }

  /**
   * Get entity form fields.
   */
  public function getFields($type) {
    $fields = $this->settingsactions->getSettings('entity_fields');
    if (isset($fields[$type])) {
      $fields = $fields[$type];
    }
    else {
      $fields = $this->getDefaultFields($type);
      if (!empty($fields)) {
        $fields = array_keys($fields);
      }
    }
    return $fields;
  }

  /**
   * Get entity fields list.
   */
  public function getDefaultFields($type) {
    $fields = [];
    switch ($type) {
      // Nodes.
      case 'nodes':
        $fields['author'] = $this->t('Authoring information');
        $fields['format'] = $this->t('Text format selection');
        $fields['options'] = $this->t('Promotion options');
        $fields['revision_information'] = $this->t('Revision information');
        $fields['url_redirects'] = $this->t('Url redirects');
        if ($this->modulehandler->moduleExists('menu_ui')) {
          $fields['menu'] = $this->t('Menu settings');
        }
        if ($this->modulehandler->moduleExists('path')) {
          $fields['path'] = $this->t('URL path settings');
        }
        if ($this->modulehandler->moduleExists('simple_sitemap')) {
          $fields['simple_sitemap'] = $this->t('XML sitemap');
        }
        if ($this->modulehandler->moduleExists('drupal_seo')) {
          $fields['drupal_seo'] = $this->t('Drupal seo');
        }
        $fields['field_comments'] = $this->t('Comments');
        break;

      // Users.
      case 'users':
        $fields['format'] = $this->t('Text format selection');
        $fields['status'] = $this->t('Status (blocked/active)');
        $fields['notify'] = $this->t('User notify');
        $fields['roles'] = $this->t('User roles');
        if ($this->modulehandler->moduleExists('path')) {
          $fields['path'] = $this->t('URL path settings');
        }
        break;

      // Comments.
      case 'comments':
        $fields['format'] = $this->t('Text format selection');
        break;

      // Taxonomy.
      case 'taxonomy':
        $fields['format'] = $this->t('Text format selection');
        $fields['relations'] = $this->t('Parents');
        if ($this->modulehandler->moduleExists('path')) {
          $fields['path'] = $this->t('URL path settings');
        }
        if ($this->modulehandler->moduleExists('simple_sitemap')) {
          $fields['simple_sitemap'] = $this->t('XML sitemap');
        }
        if ($this->modulehandler->moduleExists('tvi')) {
          $fields['tvi'] = $this->t('Tvi');
        }
        break;

      // Blocks.
      case 'blocks':
        $fields['format'] = $this->t('Text format selection');
        $fields['revision_information'] = $this->t('Revision information');
        break;
    }

    // Alter hide form fields.
    $this->modulehandler->alter('simplifying_get_fields', $fields, $type);
    return $fields;
  }

}
