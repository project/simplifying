<?php

namespace Drupal\simplifying\Services;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class SettingsActions.
 *
 * @package Drupal\simplifying
 */
class SettingsActions {

  /**
   * Add ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configfactory;

  /**
   * Add __construct.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configfactory
   *
   *   Add ConfigFactory.
   */
  public function __construct(ConfigFactory $configfactory) {
    $this->configfactory = $configfactory;
  }

  /**
   * Get simplifying settings.
   */
  public function getSettings($file) {
    static $settings;
    if (empty($settings[$file])) {
      $settings[$file] = $this->configfactory->get('simplifying.settings')->get($file);
      if (isset($settings[$file])) {
        $settings[$file] = unserialize($settings[$file]);
      }
      else {
        $settings[$file] = $this->getDefaultsSettings($file);
      }
    }
    return $settings[$file];
  }

  /**
   * Set simplifying settings.
   */
  public function setSettings($file, $data = []) {
    $this->configfactory->getEditable('simplifying.settings')
      ->set($file, serialize($data))
      ->save();
  }

  /**
   * Get defaults simplifying settings.
   */
  public function getDefaultsSettings($file) {
    $defaults = [
      'design' => [
        'small_button' => 0,
        'top_background' => '#50ab09',
        'top_color' => '#ffe30b',
        'submenu_background' => '#d6e1ea',
      ],
      'toolbar_tabs' => [
        'devel' => 'devel',
        'home' => 0,
        'administration' => 0,
        'shortcuts' => 0,
        'user' => 0,
        'contextual' => 0,
      ],
      'menu_links' => [
        'admin/appearance/settings/personal',
        'admin/appearance/settings/seven',
        'admin/index',
        'admin/flush/views',
        'admin/flush/cssjs',
        'admin/flush/twig',
        'admin/flush/plugin',
        'admin/flush/rendercache',
        'admin/flush/static-caches',
        'admin/flush/menu',
        'admin/config/development',
        'update.php',
        // 'media/add',
        'admin/content/files',
        'admin/structure/paragraphs_type',
        'admin/structure/views',
        'admin/structure/menu',
        'admin/structure/display-modes',
        'admin/structure/block',
        'admin/structure/taxonomy/add',
        'admin/structure/comment',
        'admin/structure/types',
        'admin/structure/media',
        'admin/appearance',
        'admin/modules',
        'admin/config/people',
        'admin/config/system/smtp',
        'admin/config/system/statistics',
        'admin/config/system/file_mdm',
        'admin/config/system/cron',
        'admin/config/user-interface',
        'admin/config/content',
        'admin/config/media',
        'admin/config/search',
        'admin/config/regional',
        'admin/config/services',
        'admin/config/texts',
        'admin/config/workflow',
        'admin/people/permissions',
        'admin/people/roles',
        'admin/reports/webform-plugins/elements',
        'admin/reports/views-plugins',
        'admin/reports/fields',
      ],
      'entity_fields' => [
        'nodes' => [
          'author' => 'author',
          'format' => 'format',
          'options' => 'options',
          'revision_information' => 'revision_information',
          'url_redirects' => 'url_redirects',
          'menu' => 'menu',
          'path' => 'path',
          'simple_sitemap' => 'simple_sitemap',
          'drupal_seo' => 'drupal_seo',
          'field_comments' => 'field_comments',
        ],
        'users' => [
          'format' => 'format',
          'status' => 'status',
          'notify' => 'notify',
          'roles' => 'roles',
          'path' => 'path',
        ],
        'comments' => [
          'format' => 'format',
        ],
        'taxonomy' => [
          'format' => 'format',
          'relations' => 'relations',
          'path' => 'path',
          'simple_sitemap' => 'simple_sitemap',
          'tvi' => 'tvi',
        ],
        'blocks' => [
          'format' => 'format',
          'revision_information' => 'revision_information',
        ],
      ],
    ];
    return isset($defaults[$file]) ? $defaults[$file] : [];
  }

}
