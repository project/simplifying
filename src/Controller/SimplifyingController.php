<?php

namespace Drupal\simplifying\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class SimplifyingController extends ControllerBase {

  /**
   * Simplifying services page.
   */
  public function services() {
    $langcode = self::getLangcode();
    $content['page'] = [
      '#theme' => 'simplifying_services',
      '#data'  => [
        'langcode' => $langcode,
        'base_url' => $GLOBALS['base_url']
      ],
    ];
    return $content;
  }

  /**
   * Simplifying training page.
   */
  public function training() {
    $langcode = self::getLangcode();
    $content['page'] = [
      '#theme' => 'simplifying_training',
      '#data'  => ['langcode' => $langcode],
    ];
    return $content;
  }

  /**
   * Simplifying get static pages langcode.
   */
  public static function getLangcode() {
    $lengcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (!in_array($lengcode, ['uk', 'ru'])) {
      $lengcode = 'en';
    }
    return $lengcode;
  }

}
