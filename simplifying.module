<?php

/**
 * @file
 * Simplifying module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function simplifying_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.simplifying':
      $text = file_get_contents(dirname(__FILE__) . '/README.txt');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_theme().
 */
function simplifying_theme($existing, $type, $theme, $path) {
  return [
    'simplifying_services' => [
      'variables' => ['data' => NULL],
    ],
    'simplifying_training' => [
      'variables' => ['data' => NULL],
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK() for page templates.
 */
function simplifying_preprocess_page(&$variables) {
  \Drupal::service('simplifying.toolbar')->entityReadingRoute();
}

/**
 * Implements hook_form_node_form_alter().
 */
function simplifying_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('simplifying.entity')->hideFields($form, 'nodes');
}

/**
 * Implements hook_form_user_form_alter().
 */
function simplifying_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('simplifying.entity')->hideFields($form, 'users');
}

/**
 * Implements hook_form_comment_form_alter().
 */
function simplifying_form_comment_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('simplifying.entity')->hideFields($form, 'comments');
}

/**
 * Implements hook_form_taxonomy_form_alter().
 */
function simplifying_form_taxonomy_term_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('simplifying.entity')->hideFields($form, 'taxonomy');
}

/**
 * Implements hook_form_block_form_alter().
 */
function simplifying_form_block_content_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('simplifying.entity')->hideFields($form, 'blocks');
}

/**
 * Implements hook_entity_insert().
 */
function simplifying_entity_insert(EntityInterface $entity) {
  try {
    \Drupal::service('simplifying.toolbar')->entityInsert($entity);
  }
  catch (\Exception $e) {}
}

/**
 * Implements hook_entity_delete().
 */
function simplifying_entity_delete(EntityInterface $entity) {
  try {
    \Drupal::service('simplifying.toolbar')->entityDelete($entity->getEntityTypeId(), $entity->bundle(), $entity->id());
  }
  catch (\Exception $e) {}
}

/**
 * Implements hook_toolbar().
 */
function simplifying_toolbar() {
  $items = [];
  \Drupal::service('simplifying.toolbar')->toolbarTabs($items);
  return $items;
}
