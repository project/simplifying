README.txt for Simplifying module.
----------------------------------

INTRODUCTION
---------------

Simplifying module allows you to make managing your website on Drupal 8 
easier and more convenient. With our module, you can remove all unnecessary 
tabs, menu items or fields of materials.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Devel module as you would normally install a contributed Drupal 
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The configuration page is at admin/config/development/simplifying, 
where you can configure the Simplifying module
