<?php

/**
 * @file
 * Document all supported APIs.
 */

/**
 * Alter to change the list of content fields to be hidden.
 *
 * @param array $fields
 *   List of content fields to be hidden.
 * @param string $type
 *   Content entity type to hide fields.
 */
function hook_simplifying_get_fields_alter(array &$fields, string $type) {
  // Comment field exclusion.
  if ($type == 'comments') {
    $fields['format'] = t('Text format selection');
  }
}

/**
 * Alter hide form fields.
 *
 * @param array $form
 *   The form in which the fields will be hidden.
 * @param string $field
 *   The name of the field to be hidden.
 */
function hook_simplifying_hide_field_alter(array &$form, string &$field) {
  // The exception of hiding the default field from the module list.
  if ($field == 'field') {
    $field = '';
  }
  // Hiding your own form field.
  if ($field == 'field2') {
    $form['my_field']['#access'] = FALSE;
  }
}

/**
 * Alter to hide the toolbar tabs.
 *
 * @param array $tabs
 *   List of toolbar tabs to be hidden.
 */
function hook_simplifying_hide_toolbar_tabs_alter(array &$tabs) {
  // Adding to the list of hiding your own tabs.
  $tabs[] = 'my_tab';
}
