(function ($, cookies) {
  'use strict';
  window.SimplifyingSwitch = function (event, obj) {
    if (!$(obj).hasClass('switch')) {
      $(obj).addClass('switch');
      var simplifying = cookies.get('simplifying');
      simplifying = parseInt(simplifying);
      if (isNaN(simplifying)) {
        simplifying = 0;
      }
      cookies.set('simplifying', (simplifying ? 0 : 1), {
        path: '/',
        expires: 365,
        domain: '.' + location.hostname
      });
      location.reload();
    }
    event.preventDefault();
    return false;
  };

  $(document).ready(function () {
    var k;
    var simplifying = cookies.get('simplifying');
    simplifying = parseInt(simplifying);
    if (isNaN(simplifying)) {
      simplifying = 0;
    }
    if (simplifying !== 1) {

      if (drupalSettings.simplifying_menu_links) {
        for (k in drupalSettings.simplifying_menu_links) {
          if ($('#toolbar-bar a[data-drupal-link-system-path="' + drupalSettings.simplifying_menu_links[k] + '"]').length) {
            $('#toolbar-bar a[data-drupal-link-system-path="' + drupalSettings.simplifying_menu_links[k] + '"]').each(function () {
              $(this).parents('li:first').addClass('hidden').hide();
              if (!$(this).parents('li:first').parents('li:first').find('ul:first > li:not(.hidden)').length) {
                $(this).parents('li:first').parents('li:first').removeClass('menu-item--expanded');
              }
            });
          }
        }
      }

      if (drupalSettings.simplifying_menu_tabs) {
        for (k in drupalSettings.simplifying_menu_tabs) {
          if ($('#toolbar-bar .toolbar-tab a#toolbar-item-' + k).length) {
            // $('#toolbar-bar .toolbar-tab a#toolbar-item-' + k).parents('.toolbar-tab:first').remove();
            $('#toolbar-bar .toolbar-tab a#toolbar-item-' + k).parents('.toolbar-tab:first').hide();
          }
        }
      }

    }

  });
})(jQuery, window.Cookies);
